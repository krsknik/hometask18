﻿#include <iostream>
#include <cassert>
#include <iomanip>


template <class T>
class Stack 
{
private:
	T* stackPtr;
	int size;
	int top;
public:
	
	
	Stack(int maxSize) : size(maxSize)
	{
		stackPtr = new T[size];
		top = 0;
	}
	
	Stack(const Stack& otherStack) : size(otherStack.getStackSize())
	{
		stackPtr = new T[size];
		top = otherStack.getTop();

		for (int ix = 0; ix < top; ix++)
			stackPtr[ix] = otherStack.getPtr()[ix];
	}
	
	void push(const T &value)
	{
		if (top >= size)
		{

			T* tempPtr = new T[size * 2];//выделяем память, которая в два раза больше
			for (int i = 0; i < size; i++)
			{
				tempPtr[i] = stackPtr[i];//копируем все элементы из старой памяти в новую
			}
			size = size * 2; //удваиваем переменную, которая хранит размер выделенной памяти
			delete[] stackPtr;//освобождаем старую память
			stackPtr = tempPtr; //обновляем указатель на новую память
		}
		stackPtr[top++] = value;
	}
	
	void pop()
	{
		assert(top > 0);
		stackPtr[--top];
	}
	
	const T& Peek(int nom) const
	{
		assert(nom <= top);
		return stackPtr[top - nom];
	}

	void printStack()
	{
		for (int ix = top - 1; ix >= 0; ix--)
			std::cout << "|" << stackPtr[ix] << std::endl;
	}

	int getStackSize() const
	{
		return size;
	}

	T* getPtr() const
	{
		return stackPtr;
	}
	
	int getTop() const
	{
		return top;
	}
};

int main()
{
	setlocale(LC_ALL, "rus");

	Stack<char> stackSymbol(5);
	int ct = 0;
	char ch;

	std::cout << "Введите пять чисел:" << std::endl;

	while (ct++ < 5)
	{
		std::cin >> ch;
		stackSymbol.push(ch);
	}

	std::cout << "\nСтек: " << std::endl;

	stackSymbol.printStack();

	std::cout << "\nУдален первый элемент из стека:\n";
	stackSymbol.pop();

	stackSymbol.printStack();
	Stack<char> newStack(stackSymbol);

	std::cout << "\nВторой в очереди элемент: " << newStack.Peek(2) << std::endl;

	return 0;
}